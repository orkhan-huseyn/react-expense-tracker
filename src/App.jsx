import { useState } from 'react';

function App() {
  const [budget, setBudget] = useState(0);
  const [isEditMode, setEditMode] = useState(false);
  const [expenses, setExpenses] = useState([]);

  function handleFormSubmit(event) {
    event.preventDefault();

    const { name, cost } = event.target.elements;
    const expense = {
      name: name.value,
      cost: Number(cost.value),
    };

    setExpenses((oldExpenses) => [...oldExpenses, expense]);

    name.value = '';
    cost.value = '';
  }

  function handleEdit() {
    setEditMode(true);
  }

  function handleSave() {
    setEditMode(false);
  }

  const spentSoFar = expenses.reduce((sum, expense) => sum + expense.cost, 0);

  return (
    <div className="container w-50">
      <h1 className="row my-4">My Budget Planner</h1>
      <div className="row gap-3">
        <div className="col p-4 rounded-1 d-flex justify-content-between align-items-center bg-secondary-subtle">
          {isEditMode ? (
            <>
              <input
                value={budget}
                onChange={(e) => {
                  setBudget(+e.target.value);
                }}
                className="form-control me-2"
              />
              <button
                onClick={handleSave}
                type="button"
                className="btn btn-primary"
              >
                Save
              </button>
            </>
          ) : (
            <>
              Budget: ${budget}
              <button
                onClick={handleEdit}
                type="button"
                className="btn btn-primary"
              >
                Edit
              </button>
            </>
          )}
        </div>
        <div className="col p-4 rounded-1 d-flex justify-content-between align-items-center bg-success-subtle">
          Remaining: ${budget - spentSoFar}
        </div>
        <div className="col p-4 rounded-1 d-flex justify-content-between align-items-center bg-primary-subtle">
          Spent so far: ${spentSoFar}
        </div>
      </div>
      <h2 className="row my-4">Expenses</h2>
      <div className="row">
        <input
          type="text"
          className="form-control"
          placeholder="Type to search..."
        />
      </div>
      <div className="row mt-3">
        <ul className="list-group p-0">
          {expenses.map((expense, index) => (
            <li key={index} className="list-group-item">
              <div className="d-flex justify-content-between align-items-center">
                {expense.name}
                <span class="badge rounded-pill text-bg-primary ms-auto me-2">
                  ${expense.cost}
                </span>
                <button
                  type="button"
                  class="btn-close"
                  aria-label="Close"
                ></button>
              </div>
            </li>
          ))}
        </ul>
      </div>
      <h2 className="row my-4">Add Expense</h2>
      <form onSubmit={handleFormSubmit}>
        <div className="row gap-2">
          <div className="col p-0 mb-3">
            <label htmlFor="name" className="form-label">
              Name
            </label>
            <input
              type="text"
              className="form-control"
              id="name"
              placeholder="Name..."
            />
          </div>
          <div className="col p-0 mb-3">
            <label htmlFor="cost" className="form-label">
              Cost
            </label>
            <input
              type="number"
              className="form-control"
              id="cost"
              placeholder="Cost..."
            />
          </div>
        </div>
        <div className="row">
          <button className="btn btn-primary">Add</button>
        </div>
      </form>
    </div>
  );
}

export default App;
